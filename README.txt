Approach:
	Creating hexagonal cells for each character and adding edges to its neighbors in upper layer and lower layer.

	For each string in the dictionary, I'm running DFS to find if the word is present or not

	Storing the words that matched

	Sorting the words before displaying

Improvements:
	The patterns that are to be searched can be preprocessed by using Aho-Corosick algorithm (basically KMP for Trie structure). This will improve the efficiency of the algorithm since we skip patterns which are redundant in nature.

Usage:
	python HoneyCombSearch.py honeycomb.txt dictionary.txt