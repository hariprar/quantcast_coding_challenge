from sets import Set
class WordsDict:
	def __init__(self, input_path):
		try:
			f = open(input_path, 'r')
		except IOError as e:
			print "I/O error({0}): {1}".format(e.errno, e.strerror)
			print "Input file of dictionary data incorrect"
			exit(0)
		self.words = Set([line.rstrip('\n') for line in f])
