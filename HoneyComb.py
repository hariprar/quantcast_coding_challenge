from HexagonalCell import HexagonalCell

class HoneyComb:
	def __init__(self, input_path):
		try:
			f = open(input_path, 'r')
		except IOError as e:
			print "I/O error({0}): {1}".format(e.errno, e.strerror)
			print "Input file of honeycomb data incorrect"
			exit(0)
		line = f.readline()
		number_of_layers = int(line)
		self.layers_dict = {}
		self.map = {}
		i = 0
		line = f.readline()
		while(line):
			line = line.replace("\n", "")
			self.layers_dict[i] = []
			for c in line:
				self.layers_dict[i].append(HexagonalCell(c, i))
			if i == 0:
				if len(self.layers_dict[i]) != 1:
					print "Number of characters in input data on layer %d is incorrect" % (i)
					exit(0)
			elif len(self.layers_dict[i]) != (6*i):
				print "Number of characters in input data on layer %d is incorrect" % (i)
				exit(0)

			i += 1
			line = f.readline()

		if len(self.layers_dict) != number_of_layers:
			print "Number of layers specified in the file does not match with number of lines of data"
			exit(0)

		self.initHoneyComb()
		self.populateMap()

	'''
	Add neighbors in same layer
		@param current layer number
	'''
	def addSameLayerNeighbors(self, curr_layer_num):
		cells = self.layers_dict[curr_layer_num]
		size = len(cells)
		if(size > 1):
			for i in xrange(size):
				curr_cell = cells[i]
				# for satisfying cyclic nature
				if i == 0:
					curr_cell.addNeighbor(cells[i+1])
					curr_cell.addNeighbor(cells[size-1])
					continue
				elif i == size - 1:
					curr_cell.addNeighbor(cells[0])
					curr_cell.addNeighbor(cells[i-1])
					continue

				# add previous cell and next cell
				curr_cell.addNeighbor(cells[i+1])
				curr_cell.addNeighbor(cells[i-1])

	'''
	Add neighbors from upper layer
		@param current layer number
		@param upper layer number
	'''
	def addUpperLayerNeighbors(self, curr_layer_num, upper_layer_num):
		upper_idx = 0 # tracking upper layer cells
		curr_layer_cells = self.layers_dict[curr_layer_num]
		upper_layer_cells = self.layers_dict[upper_layer_num]

		curr_layer_size = len(curr_layer_cells)
		upper_layer_size = len(upper_layer_cells)

		# iterate through all cells in current layer and add neighbors from upper layer
		for curr_idx in xrange(curr_layer_size):
			cell = curr_layer_cells[curr_idx]
			# last cell in upper layer will be neighbor of first cell in current layer
			if curr_idx == 0:
				cell.addNeighbor(upper_layer_cells[upper_layer_size-1])

			# special case: only layer 2 will have 3 neighbors from upper layer
			if curr_layer_num == 1:
				cell.addNeighbor(upper_layer_cells[upper_idx])
				upper_idx += 1

				if curr_idx != 0:
					cell.addNeighbor(upper_layer_cells[upper_idx])
					upper_idx += 1

				cell.addNeighbor(upper_layer_cells[upper_idx])
				continue

			cell.addNeighbor(upper_layer_cells[upper_idx])
			upper_idx += 1
			cell.addNeighbor(upper_layer_cells[upper_idx])
			# cells at 5 cornors of the layer, excluding the top cell will have 3 neighbors
			if curr_idx % curr_layer_num == 0 and curr_idx != 0:
				upper_idx += 1
				cell.addNeighbor(upper_layer_cells[upper_idx])

	'''
	Add neighbors from lower layer
		@param current layer number
		@param lower layer number
	'''
	def addLowerLayerNeighbors(self, curr_layer_num, lower_layer_num):
		lower_idx = 0
		curr_layer_cells = self.layers_dict[curr_layer_num]
		lower_layer_cells = self.layers_dict[lower_layer_num]

		curr_layer_size = len(curr_layer_cells)
		lower_layer_size = len(lower_layer_cells)

		for curr_idx in xrange(curr_layer_size):
			cell = curr_layer_cells[curr_idx]
			cell.addNeighbor(lower_layer_cells[lower_idx])
			# cells which are not at the cornor will have 2 neighbors
			if ((curr_idx % curr_layer_num != 0) and (curr_idx !=0)):
				# cyclic nature special case
				if curr_idx == curr_layer_size - 1:
					cell.addNeighbor(lower_layer_cells[0])
					continue
				lower_idx += 1
				cell.addNeighbor(lower_layer_cells[lower_idx])
	'''
	Utility function to print honeycomb structure
		@param current layer number
		@param next layer number
	'''
	def printHoneyComb(self):
		for k in self.layers_dict:
			for cell in self.layers_dict[k]:
				print "layer %d neighbors of %s" % (k, cell.value)
				print [neighbor.value for neighbor in cell.neighbors]
				print [neighbor.level for neighbor in cell.neighbors]

	'''
	Initialize honeycomb structure
	'''
	def initHoneyComb(self):
		layers_dict = self.layers_dict
		num_layers = len(layers_dict)
		for k in layers_dict:
			self.addSameLayerNeighbors(k)
			if k == 0:
				if num_layers > 1:
					layers_dict[k][0].addNeighbors(layers_dict[k+1])
				continue
			elif k == 1:
				for c in layers_dict[k]:
					c.addNeighbors(layers_dict[0])

			if k < (num_layers-1):
				self.addUpperLayerNeighbors(k, k+1)
			if k > 1:
				self.addLowerLayerNeighbors(k, k-1)
	'''
	Map containing cell location of all possible letters
		@param current layer number
		@param next layer number
	'''
	def populateMap(self):
		layers_dict = self.layers_dict
		for k in layers_dict:
			for cell in layers_dict[k]:
				c = cell.value
				if c in self.map:
					self.map[c].append(cell)
				else:
					self.map[c] = []
					self.map[c].append(cell)

