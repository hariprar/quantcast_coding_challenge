'''
Class to hold hexagonal cell structure
'''
class HexagonalCell:
	def __init__(self, data, lvl):
		self.value = data
		self.level = lvl
		self.state = 0
		self.neighbors = []

	def getValue(self):
		return self.value

	def setValue(self, data):
		self.value = data

	def getState(self):
		return self.state

	def setState(self, st):
		self.state = st

	def getLevel(self):
		return self.level

	def setLevel(self, lvl):
		self.level = lvl

	def getNeighbors(self):
		return self.neighbors

	def setNeighbors(self, new_neighbors):
		self.neighbors = neighbors

	def addNeighbors(self, new_neighbors):
		for cell in new_neighbors:
		 	# print "addNeighbors", self.value, cell.value
		 	self.neighbors.append(cell)

	def addNeighbor(self, cell):
		# print self.value, cell.value
		self.neighbors.append(cell)

	def print_cell(self):
		print "value:", self.value, "level:", self.level, "state:", self.state
		print "neighbors:", [neighbor.value for neighbor in self.neighbors]