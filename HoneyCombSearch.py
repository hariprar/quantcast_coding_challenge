'''
Word search in honeycomb structure
'''
import sys
from HoneyComb import HoneyComb
from WordsDict import WordsDict

global results
results = []

'''
Runs DFS on honeycomb structure to find target word
	@param hexagonal cell to inspect
	@param string with target word
	@param string with remaining letters
'''
def dfs(cell, target, remaining):
	if cell.getValue() == remaining[0]:
		# cell.print_cell()
		# Mark visited
		cell.setState(1)
		remaining = remaining[1:]
		# print "remaining", remaining
		if len(remaining) == 0:
			# Target word found
			results.append(target)
			return

		neighbors = cell.getNeighbors()
		for neighbor in neighbors:
			#print "neighbors of ", cell.value, "are", neighbor.value, neighbor.state
			# Run dfs on unvisited neighbors
			if neighbor.getState() == 0:
				dfs(neighbor, target, remaining)
	else:
		return

'''
Starts dfs for each word from dictionary.
	@param possible hexagonal cells to initiate dfs
	@param target word from dictionary
	@param honeycomb structure layers data
'''
def check(start_cells, target, cell_data):
	if start_cells is not None:
		for cell in start_cells:
			num_results_before_dfs = len(results)
			dfs(cell, target, target)
			# reset the cell values to 0
			# getting ready to check for next word
			for k in cell_data:
				for c in cell_data[k]:
					c.setState(0)
			num_results_after_dfs = len(results)
			if num_results_before_dfs > num_results_after_dfs:
				break
	return

'''
Initiate search for each word from input dictionary
	@param honeycomb structure to be searched
	@param dictionary structure containin the words
'''
def wordSearch(honeyComb, dictionary):
	for word in dictionary.words:
		# print "running dfs for ", word
		start_cells = None
		if word[0] in honeyComb.map:
			start_cells = honeyComb.map[word[0]]
		check(start_cells, word, honeyComb.layers_dict)
		# print results

'''
Intialize honeycomb and dictionary structures from the input
'''
def main(argv):
	if len(argv) != 2:
		print "Invalid arguments"
		exit(0)

	honeyComb = HoneyComb(argv[0])
	dictionary = WordsDict(argv[1])
	# honeyComb.printHoneyComb()
	wordSearch(honeyComb, dictionary)
	answer = set(results)
	for result in sorted(answer):
		print result

if __name__ == "__main__":
	main(sys.argv[1:])
